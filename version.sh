#!/bin/sh
set -e
set +x

VERSION_SEED="$1"

main_function(){
    BRANCH="$(git rev-parse --abbrev-ref HEAD)"

    case "$BRANCH" in
            master)
                TAG_PREFIX='stable_'
                ;;
            develop)
                TAG_PREFIX='develop_'
                ;;
            *)
                TAG_PREFIX='validation_'
                ;;
    esac

    if git tag | grep -P '^\d+\.\d+$' > /dev/null; then
        write_log "ERROR" "Depricated tag versioning detecte. Breaking the build."
        write_log "INFO" "Delete depricated tag versioning and try again (tags like '1.0' should be deleted while tags like 'develop_1.0' should be kept)."
        exit 254
    else
        LATEST_VERSION_ON_BRANCH="$(git tag -l "${TAG_PREFIX}*" --sort=-v:refname | head -n 1 || true)"
    fi

    CURRENT_VERSION="${LATEST_VERSION_ON_BRANCH##*_}"

    if [ -n "$CURRENT_VERSION" ]; then
        write_log "INFO" "Current Version Found: $CURRENT_VERSION"

        CURRENT_TAG_SHA="$(git rev-list -n 1 "refs/tags/${TAG_PREFIX}${CURRENT_VERSION}" 2> /dev/null || true)"

        if [ -z "$CURRENT_TAG_SHA" ]; then
            write_log "INFO" "Tag SHA is NULL."
        else
            write_log "INFO" "Tag SHA is $CURRENT_TAG_SHA"
        fi

        HEAD_SHA="$(git rev-parse HEAD || true)"
        write_log "INFO" "HEAD SHA commit is $HEAD_SHA"

        if [ "$HEAD_SHA" != "$CURRENT_TAG_SHA" ]; then
            write_log "INFO" "SHA Change Detected. Incrementing Minor Version"
            NEW_VERSION="$(increment_currentvVersion "$CURRENT_VERSION")"
            VERSION_CHANGED=true
        fi

    else
        write_log "INFO" "CURRENT_VERSION was not set.  Setting it now to the seed value (${VERSION_SEED})."
        NEW_VERSION="$VERSION_SEED"
        VERSION_CHANGED=true
    fi

    if [ "$VERSION_CHANGED" = true ]; then
        write_log "INFO" "NOW current version is ${NEW_VERSION}"

        echo "${TAG_PREFIX}${NEW_VERSION}" > version.txt

        cat > version.json << EOF
{
    "version": "$NEW_VERSION",
    "branch": "$BRANCH",
    "tag_prefix": "$TAG_PREFIX"
}
EOF
    else
        write_log "INFO" "Creating nochange file."
        echo "${TAG_PREFIX}${CURRENT_VERSION}" > version.txt
        touch nochange
    fi
}

increment_currentvVersion(){
    CURRENT_VERSION="$1"

    MAJOR_VERSION="$(echo "$CURRENT_VERSION" | awk -F '.' '{print $1}')"
    MINOR_VERSION="$(echo "$CURRENT_VERSION" | awk -F '.' '{print $2}')"

    NEW_MINOR_VERSION="$((MINOR_VERSION+1))"

    NEW_VERSION="${MAJOR_VERSION}.${NEW_MINOR_VERSION}"

    echo "$NEW_VERSION"
}

write_log(){
    LOGGING="$1"
    MESSAGE="$2"
    TIMESTAMP="[$(date)]"

    echo "$TIMESTAMP - $LOGGING - $MESSAGE"
}

main_function
